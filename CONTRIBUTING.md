# Contribute guide

## Install development environment

- [Install Poetry](https://python-poetry.org/docs/#installation)
- Install DuniterPy runtime and dev (test) dependencies:

```bash
poetry install
```

## Development workflow

Create and push a feature or bugfix release out of the `main` branch.
Name this branch after the ticket number `###`.
Open a merge request out of this branch to the `main` branch:

```bash
main <− ###_feature_branch
```

## Pre-commit

We are using [`pre-commit`](https://pre-commit.com/) tool to perform checks on staged changes before committing.
We are using it for `black` formatting, `isort` imports sorting, `pylint` code linting, `pyupgrade` syntax upgrader, `mypy` typing, and `gitlab-ci` linting.

Install `pre-commit` from your distribution. In case it is an outdated version, install it from `pip`:

```bash
sudo apt install pre-commit
pip install --user pre-commit
```

To install the `git-hooks`, run:

```bash
duniterpy> pre-commit install
```

Then each time you commit changes, the hooks will perform checks.

To manually run one of the tool above, run (eg for `isort`):

```bash
duniterpy> pre-commit run --all-files isort
```

To run all checks on all files:

```bash
duniterpy> pre-commit run -a
```

### Authorization for GitLab CI linter hook

`pre-commit run -a (gitlab-ci-linter)` is failing due to authorization required for CI lint API accesses.
When running this command, just ignore this failed hook.
In case you want to commit a `.gitlab-ci.yml` edition, this hook will prevent the commit creation.
You can [skip the hooks](https://stackoverflow.com/a/7230886) with `git commit -m "msg" --no-verify`.
This is fine for occasional `.gitlab-ci.yml` editions. In case you would like to edit this file more often and have it checked, ask a maintainer to provide you with `GITLAB_PRIVATE_TOKEN` environment variable that can be set into a shell configuration.
With Bash, in `$HOME/.bashrc` add the following:

```bash
export GITLAB_PRIVATE_TOKEN=""
```

With Fish, in `$HOME/.config/fish/config.fish` add the following:

```fish
set -xg GITLAB_PRIVATE_TOKEN ""
```

Check out #169 for more details.

## Tests

We are using [`pytest` framework](https://docs.pytest.org/).

- Run all tests with:

```bash
duniterpy> poetry run pytest
```

- Run specific tests by specifying the path to a file:

```bash
duniterpy> poetry run pytest tests/helpers/test_ws2p.py
```

- You can even specify a test from the selected file:

```bash
duniterpy> poetry run pytest tests/helpers/test_ws2p.py::test_generate_ws2p_endpoint
```

### Update copyright year

Follow [this documentation](https://github.com/Lucas-C/pre-commit-hooks#removing-old-license-and-replacing-it-with-a-new-one)
Only difference is to update the year in `license_header.txt` rather than `LICENSE.txt`.

## Manual packaging and deployment to PyPI test and PyPI repositories

Change and commit and tag the new version number (semantic version number)

```bash
./release.sh 1.2.0
```

Build the PyPI package in the `dist` folder

```bash
make build
```

Deploy the package to PyPI test repository:

```bash
make deploy_test
```

Install DuniterPy from PyPI test repository and its dependencies from PyPI:

```bash
pip install --index-url https://test.pypi.org/simple/ --extra-index-url https://pypi.python.org/simple/ duniterpy
```

Deploy the package on PyPI repository:

```bash
make deploy
```

## Release workflow

To handle a release, you have to follow this workflow:

- Verify all features and bug fixes are merged in the `main` branch.
- Checkout on the `main` branch
- Update the `CHANGELOG.md` file and commit
- Run the `release.sh` script with the version semantic number as argument:

```bash
./release.sh 0.50.0
```

- Create a MR containing the changelog and the commit updating the version
- Once the MR is merged to `main`, push the tag with `git push --tags`
- To release to PyPI, the pipeline based on the tag should be used by triggerring the manual job on the `main` branch

### How to generate and read locally the autodoc

- Install Sphinx, included into the `doc` dependencies group:

```bash
poetry install --with doc
```

- Generate HTML documentation in `public` directory:

```bash
make docs
```

## Docstrings

- When writing docstrings, use the reStructuredText format recommended by https://www.python.org/dev/peps/pep-0287/#docstring-significant-features
