.. duniterpy documentation master file, created by
   sphinx-quickstart on Tue Oct  6 16:34:46 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

duniterpy : A python implementation of duniter API
==================================================

DuniterPy is a library to develop an client Python application for Duniter.
DuniterPy helps to handle the following problem:

* Request Basic Merkle API provided by duniter nodes
* Request nodes in a non-blocking way
* Handle duniter signing keys

Requirements
------------

DuniterPy requires Python 3.9.0 minimum.

Installation
------------

Simply type::

    $ pip3 install duniterpy

Source code
-----------

Sources can be found at https://git.duniter.org/clients/python/duniterpy

Contributions are welcome.


Contents:
=========

.. toctree::
   :glob:

   modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

